![](https://i.imgur.com/wh2QXAt.png)

## Avalith Front End Development Challenge

Welcome! Thanks in advance for the time and interest in joining the Avalith team. Here you'll find a challenge specifically oriented to showcase some of your coding (and maybe other) skills while working with a similar set of tools to the ones we use on a daily basis, in an intuitive and design-based approach. 
We think that this is somewhat more pragmatic and spontaneous than getting you to code live on a website with a clock ticking ⏱ (and probably you do too).

### Overview

The goal of the assignment is to create a Login with [firebase](https://firebase.google.com/) build in a [Single Page Application](https://es.wikipedia.org/wiki/Single-page_application) that consists of two main screens:

* A **Login screen** with two login methods (email and google account), take care of display login error message.
* A **Info screen** showcasing email, photo, last login of the user and a logout button .

Mobile layout is mandatory, you can use your favorite tool for the task.

### Tech Stack & Requirements

At Avalith, our main stack is currently based on **React & Redux** 💎, but ultimately, feel free to use any set of technologies you feel comfortable / most proficient with, such as Angular, VueJS. You'll most likely be called in for a brief showcase of your final output and walk through some technical decisions. Keep that in mind when thinking of implementing utility libraries such as Lodash or Ramda (that's fine as long as you can explain what you're doing afterwards).

SASS, LESS and CSS in JS are welcome as well.

(It's your party, try to keep it modern) 🤘

### What we are looking for

We pay special attention to:

- A basic README file with information about the project (e.g. how to install dependencies, how to run it locally). Make sure these commands actually work! You'd be amazed at how many previous challenges didn't get past the **yarn start** phase.
- How stylesheets and class names are structured. Is there any special methodology? ([BEM](http://getbem.com/introduction/) is your friend). Is it reusable? Scalable?
- Code structure (modularity, dependencies, scaffolding, documentation, etc)
- Semantic HTML and whether it's being used properly along with CSS & JavaScript
- Discussions are encouraged. We chat regularly with other engineers and design teams as part of the daily workflow. Don't hesitate on asking questions as we're going to check if things work as the requirements describe after all.
- State management, if any libraries or patters are used, readability, usage of modern front end technologies and best practices.
- Edge cases (think about. What happens if a user gets into to de info page _by route_? or if the conexion with firebase fail during logout) These are all very important on a day-to-day project workflow.
- [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself), [SOLID](https://en.wikipedia.org/wiki/SOLID), [KISS](https://en.wikipedia.org/wiki/KISS_principle) and [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it), if you find them applicable (can't go wrong with any of them)

### Bonus points
If you fancy doing a little extra, here are some things you could spend your time on:

- **Animations**, as it's 2020 already! Page transitions / hover states are always a plus, maybe somes loading spinners? 
- **Deployment**. Hosting on _**Heroku**_ or _**Firebase**_  is very much apprecciated and saves everyone's time (It's also terribly easy nowadays).
- **Storage**. you can go crazy with realtime database, an for example give your users the ability of create a todo list! choose what you like most and have fun! 

### That's all!

Happy hacking 🎉 

![Happy Hacking](https://media.tenor.com/images/df8c44a1d20ab367fdcb21880985fd33/tenor.gif)
